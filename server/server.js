//variables del entorno
require('./config/config');

// Framework Express
const express = require('express');
// Renombramos la libreria express para ocuparla con app
const app = express();

// libreria de mongoose para conexiones con mongoDB
const mongoose = require('mongoose');

//Libreria para leer el body(paramentros del formulario) de la peticion que se envia desde el Frond-End
const bodyParser = require('body-parser');

//Middlewares
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

const path = require('path')
//Habilitar la carpeta publica para mostrar html
app.use(express.static(path.resolve(__dirname, '../public')));
//console.log(path.resolve(__dirname, '../public'));

// parse application/json
app.use(bodyParser.json());

//Configuracion global de rutas -- Archivo de nuestras rutas donde estan las url
app.use(require('./Routes/index'));

mongoose.connect(
	process.env.URLDB,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
		useCreateIndex: true,
	},
	(err, res) => {
		if (err) throw err;
		console.log('Base de datos ONLINE');
	}
);

//puerto de la compu donde se ejecuta la aplicacion
app.listen(process.env.PORT, () => {
	console.log('Escuchando el puerto', process.env.PORT);
});
