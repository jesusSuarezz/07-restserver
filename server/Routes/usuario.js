// Libreria de Expressjs
const express = require('express');
const app = express();
// Libreria para encriptar contraseñas Solo funciona con versiones nodeLTS
const bcrypt = require('bcrypt');
//libreria para recibir solo informacion necesaria del cliente y otras cosas ...
const _ = require('underscore');
const Usuario = require('../models/usuario-model');

// Middleware para la authentication
const {
	verificarToken,
	verificarAdmin_Role,
} = require('../middlewares/authenticacion');

app.get('/usuarios', verificarToken, (request, response) => {
	let desde = request.query.desde || 0;
	desde = Number(desde); //transformamos a numero el string para que lo acepte el method skip

	let limite = Number(request.query.limite || 5);

	Usuario.find({ estado: true }, 'nombre email role google img estado')
		.skip(desde)
		.limit(limite)
		.exec((error, usuarios) => {
			if (error) {
				response.status(400).json({
					ok: false,
					error,
				});
			}

			Usuario.count({ estado: true }, (error, conteo) => {
				//el status de la API se envia por defult a 200
				response.json({
					ok: true,
					numUsuarios: conteo,
					usuarios: usuarios,
				});
			});
		});
});

app.post(
	'/usuarios',
	[verificarToken, verificarAdmin_Role],
	(request, response) => {
		let body = request.body;

		let usuario = new Usuario({
			nombre: body.nombre,
			email: body.email,
			password: bcrypt.hashSync(body.password, 10),
			role: body.role,
		});

		usuario.save((err, usuarioDB) => {
			if (err) {
				return response.status(400).json({
					ok: false,
					err: err,
				});
			}

			//usuarioDB.password = null;

			response.status(200).json({
				ok: true,
				usuario: usuarioDB,
			});
		});
	}
);

app.put(
	'/usuarios/:id',
	[verificarToken, verificarAdmin_Role],
	function (request, response) {
		// Recibimos el id de la url
		let id = request.params.id;
		// Recibimos la informacion del form
		let body = _.pick(request.body, [
			'nombre',
			'email',
			'img',
			'role',
			'estado',
		]);

		Usuario.findByIdAndUpdate(
			id,
			body,
			{ new: true, runValidators: true },
			(error, usuarioDB) => {
				if (error) {
					return response.status(400).json({
						ok: false,
						error,
					});
				}

				response.status(200).json({
					ok: true,
					ursuario: usuarioDB,
				});
			}
		);
	}
);

app.delete(
	'/usuarios/:id',
	[verificarToken, verificarAdmin_Role],
	function (request, response) {
		let id = request.params.id; //recibimos el id de la url

		let cambiaEstado = {
			estado: false,
		};

		//Usuario.findByIdAndRemove(id, (error, usuarioBorrado) => {
		Usuario.findByIdAndUpdate(
			id,
			cambiaEstado,
			{ new: true },
			(error, usuarioBorrado) => {
				if (error) {
					return response.status(400).json({
						ok: false,
						error,
					});
				}

				//Si usuarioBorrado no existe
				if (!usuarioBorrado) {
					return response.status(400).json({
						ok: false,
						error: {
							message: 'Usuario no encontrado',
						},
					});
				}
				response.json({
					ok: true,
					usuario: usuarioBorrado,
				});
			}
		);
	}
);

// Exportamos el app de la libreria express que declaramos arriba
module.exports = app;
