// Framework Express
const express = require('express');
// Renombramos la libreria express para ocuparla con app
const app = express();

//Definicion de los archivos donde se encuentran las rutas disponibles para cunsumir la API
app.use(require('./usuario'));
app.use(require('./login'));
app.use(require('./categoria'));
app.use(require('./producto'));
//Rutas de los archivos
app.use(require('./uploads'));
//Rutas de las imagenes
app.use(require('./imagenes'));

module.exports = app;
