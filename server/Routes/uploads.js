const express = require('express');
const fileUpload = require('express-fileupload');

const app = express();

const Usuario = require('../models/usuario-model');
const Producto = require('../models/producto-model');

//El modulo que nos permite entrar a los archivos del sistema
const fs = require('fs');
//El path para acceder al path
const path = require('path');
// default options
app.use(fileUpload());

//Funcion que borra archivos segun el path
const borrarArchivo = (nombreImagen, tipo) => {
	//Este es el path para  buscar la imagen que vamos a borrar al cambiar la nueva imagen
	let pathImagen = path.resolve(
		__dirname,
		`../../uploads/${tipo}/${nombreImagen}`
	);
	//Revisamos si el path de la imagen existe
	if (fs.existsSync(pathImagen)) {
		//Borramos el archivo que esta en el pathImagen
		fs.unlinkSync(pathImagen);
	}
};

//Funcion para actuallizar la nueva imagen del usuario
const imagenUsuario = (id, response, nombreArchivo) => {
	Usuario.findById(id, (error, usuarioDB) => {
		if (error) {
			//Aunque pueda ocurrir un error se pudo haber subido la imagen y la tenemos que borrar
			borrarArchivo(nombreArchivo, 'usuarios');
			return response.status(500).json({
				ok: false,
				error,
			});
		}

		if (!usuarioDB) {
			//Aunque pueda ocurrir un error se pudo haber subido la imagen y la tenemos que borrar
			borrarArchivo(nombreArchivo, 'usuarios');
			return response.status(500).json({
				ok: false,
				error: {
					message: 'El usuario no existe',
				},
			});
		}

		borrarArchivo(usuarioDB.img, 'usuarios');

		usuarioDB.img = nombreArchivo;
		usuarioDB.save((error, usuarioGuardado) => {
			response.json({
				ok: true,
				usuario: usuarioGuardado,
				img: nombreArchivo,
			});
		});
	});
};

const imagenProducto = (id, response, nombreArchivo) => {
	Producto.findById(id, (error, productoDB) => {
		//Aunque pueda ocurrir un error se pudo haber subido la imagen y la tenemos que borrar
		if (error) {
			borrarArchivo(nombreArchivo, 'productos');
			return response.status(500).json({
				ok: false,
				error,
			});
		}

		if (!productoDB) {
			//Aunque pueda ocurrir un error se pudo haber subido la imagen y la tenemos que borrar
			borrarArchivo(nombreArchivo, 'productos');
			return response.status(400).json({
				ok: false,
				error: {
					message: 'El producto no se encontro',
				},
			});
		}

		borrarArchivo(productoDB.img, 'productos');

		productoDB.img = nombreArchivo;
		productoDB.save((error, productoGuardado) => {
			response.json({
				ok: true,
				producto: productoGuardado,
			});
		});
	});
};

//Este es el servicio que servira para actualizar la imagen del usuario o producto
app.put('/upload/:tipo/:id', (request, response) => {
	let tipo = request.params.tipo;
	let id = request.params.id;

	//Si no vienen archivos
	if (!request.files || Object.keys(request.files).length === 0)
		return response.status(400).json({
			ok: false,
			error: { message: 'No se ha selecciondado ningún archivo' },
		});

	//Validar si se cambiara la img en un producto o en un usuario
	let tiposValidos = ['productos', 'usuarios'];
	if (tiposValidos.indexOf(tipo) < 0) {
		return response.status(400).json({
			ok: false,
			error: {
				message: 'Los tipos permitidos son ' + tiposValidos.join(' , '),
			},
		});
	}

	//Este es el archivo que mandamos del front-end
	let archivo = request.files.archivo;
	//Separamanos la terminacion del arcivo (el nombre y el tipo de documento)
	let nombreArchivoCortado = archivo.name.split('.');
	//Sacamos la extencion del archivo
	let extension = nombreArchivoCortado[nombreArchivoCortado.length - 1];
	//console.log(extension);

	//Validacion del tipo de archivo
	let extencionesValidas = ['png', 'jpg', 'gif', 'jpeg'];

	if (extencionesValidas.indexOf(extension) < 0) {
		return response.status(400).json({
			ok: false,
			error: {
				message:
					'Las extenciones permitidas son ' + extencionesValidas.join(', '),
				ext: extension,
			},
		});
	}

	// Cambiar el nombre del archivo a uno unico concatenando mas valores
	let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extension}`;

	//Movemos el archivo donde lo guardaremos
	//Esto quiere decir que desde aqui ya se esta guardando el archivo
	archivo.mv(`uploads/${tipo}/${nombreArchivo}`, (error) => {
		if (error) {
			return response.status(500).json({
				ok: false,
				error,
			});
		}

		//Aqui, ya se cargo mi imagen en mis archivos del servidor (fs-filesystem)
		if (tipo === 'usuarios') {
			imagenUsuario(id, response, nombreArchivo);
		} else {
			imagenProducto(id, response, nombreArchivo);
		}
	});
});

module.exports = app;
