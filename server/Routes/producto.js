const express = require('express');

let { verificarToken } = require('../middlewares/authenticacion');

const Producto = require('../models/producto-model');
let app = express();

//Obtiene todos los producto
app.get('/productos', verificarToken, (request, response) => {
	let desde = Number(request.query.desde || 0);
	let limite = Number(request.query.limite || 15);
	console.log(desde);
	//populate: usuario, categoria
	//paginado

	//pulate pasa los datos del usuario que lo dio de alta con ayuda del modelo
	//sort para ordenarlo por ese orden
	//Un usuario que empiece DESDE el numero 5 y que traiga de LIMITE solo 10 productos
	Producto.find({ disponible: true })
		.skip(desde)
		.limit(limite)
		.sort('nombre')
		.populate('usuario', 'nombre email')
		.populate('categoria', 'descripcion')
		.exec((error, productosDB) => {
			if (error) {
				return response.status(500).json({
					ok: false,
					error,
				});
			}

			response.status(200).json({
				ok: true,
				producto: productosDB,
			});
		});
});

app.get('/productos/:id', verificarToken, (request, response) => {
	//populate: usuario, categoria
	let id = request.params.id;

	Producto.findById(id)
		.populate('usuario', 'nombre email')
		.populate('categoria', 'descripcion')
		.exec((error, productoDB) => {
			if (error) {
				return response.status(500).json({
					ok: false,
					error,
				});
			}

			//Si no existe el ID del producto
			if (!productoDB) {
				return response.status(400).json({
					ok: false,
					error: {
						message: 'No se encontro el producto',
					},
				});
			}

			response.status(200).json({
				ok: true,
				producto: productoDB,
			});
		});
});

// ======================
// Buscar Productos
// ======================
app.get('/productos/buscar/:termino', verificarToken, (request, response) => {
	let termino = request.params.termino;

	//Creamos la expresion regular y con i le decimos que no sea sencible a mayusculas y minisculas
	let regex = RegExp(termino, 'i');

	Producto.find({ nombre: regex })
		.populate('categoria', 'nombre')
		.exec((error, productosDB) => {
			if (error) {
				return response.status(500).json({ ok: false, error });
			}

			response.json({
				ok: true,
				productos: productosDB,
			});
		});
});

app.post('/productos', verificarToken, (request, response) => {
	//Guardar un usuario
	//Guardar una categoria del listado

	let body = request.body;
	//console.log(request);

	//El usaurio lo crea por defacto en el modelo
	let producto = new Producto({
		nombre: body.nombre,
		precioUnitario: body.precioUnitario,
		descripcion: body.descripcion,
		disponible: body.disponible,
		categoria: body.categoria,
	});

	producto.save((error, productoDB) => {
		if (error) {
			return response.status(500).json({
				ok: false,
				error,
			});
		}

		response.status(201).json({
			ok: true,
			producto: productoDB,
		});
	});
});

app.put('/productos/:id', verificarToken, (request, response) => {
	let id = request.params.id;

	let body = request.body;
	console.log(body);

	let producto = {
		nombre: body.nombre,
		precioUnitario: body.precioUnitario,
		descripcion: body.descripcion,
		categoria: body.categoria,
	};

	Producto.findByIdAndUpdate(
		//Le dicimos quequeremos que nos envie el productoActualizado y que corra las validaciones
		id,
		producto,
		{ new: true, runValidators: true },
		(error, productoDB) => {
			console.log(productoDB);
			if (error) {
				return response.status(500).json({
					ok: false,
					error,
				});
			}

			//Error por que el lproducto.id no se encontro
			if (!productoDB) {
				return response.status(400).json({
					ok: false,
					error: {
						message: 'El id del producto no se encontro',
					},
				});
			}

			response.status(200).json({
				ok: true,
				producto: productoDB,
			});
		}
	);
});

app.delete('/productos/:id', verificarToken, (request, response) => {
	// Borrado logico

	let id = request.params.id;

	Producto.findById(id, (error, productoDB) => {
		if (error) {
			return response.status(500).json({
				ok: false,
				error,
			});
		}

		if (!productoDB) {
			return response.status(400).json({
				ok: false,
				error: {
					message: 'El producto no se encontro',
				},
			});
		}

		productoDB.disponible = false;
		productoDB.save((error, productoEliminado) => {
			if (error) {
				return response.status(500).json({
					ok: false,
					error,
				});
			}

			response.json({
				ok: true,
				producto: productoEliminado,
				message: 'Producto eliminado',
			});
		});
	});
});

// Middleware para manejar error 400
//Esto se ejecuta cada vez que hay una respuesta con error 400 y no esta controlada
app.use((error, request, response, next) => {
	if (!error) return next();
	return response.status(400).json({
		ok: false,
		error: {
			message: 'Peticion no valida',
			error: 'BAD_REQUEST',
		},
	});
});

module.exports = app;
