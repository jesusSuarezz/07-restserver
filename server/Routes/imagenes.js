const express = require('express');

//Para hacer el path absoluto
const path = require('path');

//Importamos el fileSystem
const fs = require('fs');

const { verificaTokenImg } = require('../middlewares/authenticacion');

let app = express();

app.get('/imagen/:tipo/:img', [verificaTokenImg], (request, response) => {
	let tipo = request.params.tipo;
	let img = request.params.img;

	let pathImagen = path.resolve(__dirname, `../../uploads/${tipo}/${img}`);

	//Si el path de la imagen existe
	if (fs.existsSync(pathImagen)) {
		//Enviamos lo que haya en ese path
		response.sendFile(pathImagen);
	} else {
		//Creamos el path absoluto que nos da la direccion de la imagen
		let noImagePath = path.resolve(__dirname, '../assets/no-image.jpg');

		response.sendFile(noImagePath);
	}
});

module.exports = app;
