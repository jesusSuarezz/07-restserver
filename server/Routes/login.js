// Framework Express
const express = require('express');
// Renombramos la libreria express para ocuparla con app
const app = express();

// Libreria para encriptar contraseñas Solo funciona con versiones nodeLTS
const bcrypt = require('bcrypt');

//Libreria para generar un Json Web Token
const jwt = require('jsonwebtoken');

//Libreria de autenticacion google
const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT_ID);

const Usuario = require('../models/usuario-model');

app.post('/login', (request, response) => {
	//Parametros del form que envian del forn-end
	let body = request.body;

	Usuario.findOne({ email: body.email }, (error, usuarioDB) => {
		//Error 500= en el servidor por que es de la base de datos
		if (error) {
			return response.status(500).json({
				ok: false,
				error: error,
			});
		}

		if (!usuarioDB) {
			//Aqui falla porque no encontro el email
			return response.status(400).json({
				ok: false,
				error: {
					message: '(Usuario) o contraseña incorrectos',
				},
			});
		}

		//Evaluamos si la contraseña que envio el usuario es igual a la de la BD
		//regresa un booleano esta condicion == Si no son iguales entonses haz esto
		if (!bcrypt.compareSync(body.password, usuarioDB.password)) {
			//Cuando falla la contraseña
			return response.status(400).json({
				ok: false,
				error: {
					message: 'Usuario o (contraseña) incorrectos',
				},
			});
		}

		//generando el JWT
		//Expira en 24 horas
		let token = jwt.sign(
			{
				usuario: usuarioDB,
			},
			process.env.SEED_JWT,
			{ expiresIn: process.env.CADUCIDAD_TOKEN }
		);

		response.status(200).json({
			ok: true,
			token: token,
			usuario: usuarioDB,
		});
	});
});

//CONFIGURACION DE GOOGLE
//Verificacion del token con la libreria de google
//Esta es un promesa
async function verify(token) {
	const ticket = await client.verifyIdToken({
		idToken: token,
		audience: process.env.CLIENT_ID, // Specify the CLIENT_ID of the app that accesses the backend
		// Or, if multiple clients access the backend:
		//[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
	});
	//CARGA UTIL DEL TOKEN (LA INFO DEL USURIO)
	const payload = ticket.getPayload();
	/* console.log(payload.name);
	console.log(payload.email);
	console.log(payload.picture); */

	//Hacemos un objeto personalizado
	return {
		nombre: payload.name,
		email: payload.email,
		img: payload.picture,
		google: true,
	};
}

//Peticion para recibir los datos de la autenticacion con google
app.post('/auth/google', async (request, response) => {
	//Obtenemos el token
	let token = request.body.idtoken;

	//llamamos la funcion de google para verificar el token y su informacion
	let googleUser = await verify(token).catch((error) => {
		return response.status(403).json({
			ok: false,
			err: error,
		});
	});

	//Buscamos el usuario en la base de datos
	Usuario.findOne({ email: googleUser.email }, (err, usuarioDB) => {
		if (err) {
			return response.status(500).json({
				ok: false,
				err: err,
			});
		}

		if (usuarioDB) {
			//Si existe ya el usuario en la DB y se registro sin google
			if (usuarioDB.google === false) {
				return response.status(400).json({
					ok: false,
					err: {
						message: 'Debe usar su autenticacion normal (sin google)',
					},
				});
			} else {
				//Si esta bien su autenticacicon con google=true creamos su JWT
				let token = jwt.sign({ usuario: usuarioDB }, process.env.SEED_JWT, {
					expiresIn: process.env.CADUCIDAD_TOKEN,
				});

				return response.status(200).json({
					ok: true,
					usuario: usuarioDB,
					token,
				});
			}
		} else {
			//Si el usuario no existe en la BD Creamos el nuevo usuario
			let usuario = new Usuario();

			usuario.nombre = googleUser.nombre;
			usuario.email = googleUser.email;
			usuario.img = googleUser.img;
			usuario.google = true;
			usuario.password = ':)';

			usuario.save((err, usuarioDB) => {
				if (err) {
					return response.status(500).json({
						ok: false,
						err,
					});
				}
				//creamos el nuevo token
				let token = jwt.sign(
					{
						usuario: usuarioDB,
					},
					process.env.SEED_JWT,
					{ expiresIn: process.env.CADUCIDAD_TOKEN }
				);
				return response.status(200).json({
					ok: true,
					token: token,
					usuario: usuarioDB,
				});
			});
		}
	});
});

module.exports = app;
