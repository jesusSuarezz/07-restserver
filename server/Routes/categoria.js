const express = require('express');

let {
	verificarToken,
	verificarAdmin_Role,
} = require('../middlewares/authenticacion');

let app = express();
let Categoria = require('../models/categoria-model');

//Muestra todas la categorias
app.get('/categoria', verificarToken, (request, response) => {
	Categoria.find({})
		.sort('descripcion')
		.populate('usuario', 'nombre email')
		.exec((error, categoriasDB) => {
			if (error) {
				return response.status(500).json({
					ok: false,
					error,
				});
			}
			response.json({
				ok: true,
				categoriasDB,
			});
		});
});

//Mostrar una categoria por ID
app.get('/categoria/:id', verificarToken, (request, response) => {
	let id = request.params.id;

	Categoria.findById(id, (error, categoriaDB) => {
		if (error) {
			return response.status(500).json({
				ok: false,
				error,
			});
		} else if (!categoriaDB) {
			return response.status(400).json({
				ok: false,
				error: {
					message: 'No se encontro la categoria',
				},
			});
		}

		response.json({
			ok: true,
			categoriaDB,
		});
	});
});

app.post('/categoria', verificarToken, (request, response) => {
	let body = request.body;

	//Creamos la instancia de categoria
	let categoria = new Categoria({
		descripcion: body.descripcion,
		usuario: request.usuario._id,
	});

	categoria.save((error, categoriaDB) => {
		if (error) {
			return response.status(500).json({
				ok: false,
				error: error,
			});
		}

		//Si no se crea la categoriaDB (por validaciones)
		if (!categoriaDB) {
			return response.status(400).json({
				ok: false,
				error,
			});
		}

		response.json({
			ok: true,
			categoria: categoriaDB,
		});
	});
});

//Actualiza la categoria solo si es administrador el usuario
app.put(
	'/categoria/:id',
	[verificarToken, verificarToken, verificarAdmin_Role],
	(request, response) => {
		let id = request.params.id;
		let body = request.body;

		let descCategoria = {
			descripcion: body.descripcion,
		};

		Categoria.findByIdAndUpdate(
			id,
			descCategoria,
			//Le dicimos quequeremos que nos envie el categoriaActualizada y que corra las validaciones
			{ new: true, runValidators: true },
			(error, categoriaDB) => {
				if (error) {
					return response.status(500).json({
						ok: false,
						error,
					});
				}

				//Si no devuelve la categoria (por validaciones)
				if (!categoriaDB) {
					return response.status(400).json({
						ok: false,
						error: {
							message: 'El id de la categoria no existe',
						},
					});
				}

				response.status(200).json({
					ok: true,
					categoria: categoriaDB,
				});
			}
		);
	}
);

//Elimin la categoria solo si el usuario es administradors
app.delete(
	'/categoria/:id',
	[verificarToken, verificarAdmin_Role],
	(request, response) => {
		//Extraemos el id de la url
		let id = request.params.id;

		Categoria.findByIdAndRemove(id, (error, categoriaDB) => {
			if (error) {
				return response.status(500).json({
					ok: false,
					error,
				});
			} else if (!categoriaDB) {
				return response.status(400).json({
					ok: false,
					error: {
						message: 'La id de categoria no existe',
					},
				});
			}

			response.status(200).json({
				ok: true,
				usuario: categoriaDB,
				message: 'Categoria eliminada',
			});
		});
	}
);

module.exports = app;
