const jwt = require('jsonwebtoken');

// ===========================
//  Verificar Token
// ============================
let verificarToken = (request, response, next) => {
	//Obteniendo el token que esta en los headers
	let token = request.get('token');

	jwt.verify(token, process.env.SEED_JWT, (error, decoded) => {
		if (error) {
			return response.status(401).json({
				ok: false,
				error: {
					message: 'Token no valido',
				},
			});
		}

		request.usuario = decoded.usuario;
		next();
	});
};

// ===========================
//  Verificar AdminRole
// ============================
let verificarAdmin_Role = (request, response, next) => {
	let usuario = request.usuario;

	if (usuario.role === 'ADMIN_ROLE') {
		next();
	} else {
		return response.status(401).json({
			ok: false,
			error: {
				message: 'El usuario no es administrador',
			},
		});
	}
};

// ===========================
//  Verifica el token para poder visualizar la imagen
// ============================
let verificaTokenImg = (request, response, next) => {
	//Obtenemos el token que viene en el query de la url '?'
	let token = request.query.token;

	jwt.verify(token, process.env.SEED_JWT, (error, decoded) => {
		if (error) {
			return response.status(401).json({
				ok: false,
				error: {
					message: 'Token no valido',
				},
			});
		}

		request.usuario = decoded.usuario;
		next();
	});
};

module.exports = {
	verificarToken,
	verificarAdmin_Role,
	verificaTokenImg,
};
