// ==================================================
//  Configurando el puerto en el que se ejecuta a app
// ==================================================
process.env.PORT = process.env.PORT || 3000;

// ==================================================
//  Enviroment (Entorno)
// ==================================================
//si no existe la variable NODE_ENV lo pasamos a embiente de desarrollo
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// ==================================================
//  Vencimiento del token por 5 dias
// ==================================================
process.env.CADUCIDAD_TOKEN = '48h';

// ==================================================
//  SEED (Semilla) de autenticacion con un ---> heroku config:set SEED_JWT='este-es-el-seed-produccion'
// ==================================================
process.env.SEED_JWT = process.env.SEED_JWT || 'este-es-el-seed-desarrollo';

// ==================================================
//  Database
// ==================================================

let urlDB;

if (process.env.NODE_ENV === 'dev') {
	urlDB = 'mongodb://localhost:27017/cafe';
} else {
	urlDB = process.env.MONGO_URL;

	//esta es la linea de arriba pero configurada con heroku para que no se vea en git
	//urlDB ='mongodb+srv://jesus_suarez:JywH5JkcEYAmSbBr@jesus.z6ddr.mongodb.net/cafe';
}

process.env.URLDB = urlDB;

// ==================================================
//  Google CLIENT ID
// ==================================================

process.env.CLIENT_ID =
	process.env.CLIENT_ID ||
	'230186722002-eaglaabfv5c1294so8iqejgsdshf12do.apps.googleusercontent.com';
